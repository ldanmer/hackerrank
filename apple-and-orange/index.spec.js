const countApplesAndOranges = require('./index');
const {describe, it} = require('@jest/globals');

describe('apple and orange', function () {
    it('should count the amount of apples and oranges which falls to Sam house', function () {
        const expectedResult = [1, 1];
        const input = [7, 11, 5, 15, [-2, 2, 1], [5, -6]];
        expect(countApplesAndOranges(...input)).toStrictEqual(expectedResult);
    });
});