const countApplesAndOranges = (s, t, a, b, apples, oranges) => {
    let applesCount = 0;
    let orangesCount = 0;
    apples.forEach(apple => {
        const applePosition = apple + a;
        if (applePosition >= s && applePosition <= t) {
            ++applesCount;
        }
    });

    oranges.forEach(orange => {
        const orangePosition = orange + b;
        if (orangePosition >= s && orangePosition <= t) {
            ++orangesCount;
        }
    });

    return [applesCount, orangesCount];
};

module.exports = countApplesAndOranges;