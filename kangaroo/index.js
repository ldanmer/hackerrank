const kangaroo = (x1, v1, x2, v2) => {
    return (v1 >= v2) && (x1 - x2) % (v2 - v1) === 0 ? 'YES' : 'NO';
};

module.exports = kangaroo;