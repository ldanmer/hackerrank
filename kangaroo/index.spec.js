const kangaroo = require('./index');
const {describe, it} = require('@jest/globals');

describe('kangaroo', () => {
    it('should return YES', () => {
        expect(kangaroo(0, 3, 4, 2,)).toEqual('YES');
    });

    it('should return NO', () => {
        expect(kangaroo(0, 2, 5, 3,)).toEqual('NO');
    });
});