const getTotalX = require('./index');
const {describe, it} = require('@jest/globals');

describe('getTotalX', () => {
    it('should return between number', () => {
        expect(getTotalX([2, 4], [16, 32, 96])).toEqual(3);
    });
});  