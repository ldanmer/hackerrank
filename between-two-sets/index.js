const getTotalX = (a, b) => {
    let validCount = 0;

    for (let x = 1; x <= Math.max(...b); x++) {
        a.every(int => (x % int === 0)) && b.every(int => (int % x === 0)) && validCount++;
    }

    return validCount;
};

module.exports = getTotalX;