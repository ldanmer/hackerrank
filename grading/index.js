const gradingStudents = (grades) => {
    return grades.reduce((result, grade) => {
        const parsedGrade = parseInt(grade, 10);
        const res = parsedGrade < 38 || parsedGrade % 5 < 3 ? parsedGrade : parsedGrade + (5 - (parsedGrade % 5));
        result.push(res);
        return result;
    }, []);
};

module.exports = gradingStudents;