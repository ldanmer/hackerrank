const gradingStudents = require('./index');
const {describe, it} = require('@jest/globals');

describe('grading', () => {
    it('should return modified array of grades', () => {
        const inputGrades = [4, 73, 67, 38, 33];
        const expectedGrades = [4, 75, 67, 40, 33];
        const resultArray = gradingStudents(inputGrades);
        expect(resultArray).toStrictEqual(expectedGrades);
    });
});